## Projet Framework CSS : 
#### Réalisé par Mourad Bounajra et Florian Bergeret. ####

Code HTML et CSS validé.

**[Télécharger le framework](https://bitbucket.org/mouradbounajra/framework-css/raw/bfd8d62d09718af4a68f2fc2cd658c3c6986cef7/css.zip)**

**Manuel rapide d'installation :**

Pour l'utilisation du framework, il faut télécharger le fichier puis l'intégrer dans votre code HTML.
```
<link rel="stylesheet" href="css/global.css">
```


**Lien vers le dépôt git :**

* https://bitbucket.org/mouradbounajra/framework-css/

**Lien webetu :**

* https://webetu.iutnc.univ-lorraine.fr/~bounajra1u/FrameworkCSS/
	
**Liste des fonctionnalités :** 

* Typographie :
    * Les titres de H1 à H6
    * Les sous-titres pour chaque titre
    * Balise de texte inline :
        * Mark
        * Del
        * Ins
        * U
        * Strong
        * Em
        * Abbr
    * Quote et blockquote :
        * Blockquote
        * Code
        * Pre
        * Kbd
    * Liste avec 3 types de puce :
        * Sans
        * Circulaire
        * Square (carré)
    * Alignements et transformation :
        * Gauche
        * Droite
        * Centré
        * Texte justifié

* Tableau :
    * Avec un style par défaut
    * Avec des lignes aux couleurs alternées
    * Avec ligne qui change de couleur au survol de la souris

* Boutons :
    * 4 tailles : small, normal, large et extra-large
    * 3 formes : par défaut, flat et fancy
    * 4 couleurs : bleu, rouge, vert et noir 

* Les alertes :
    * Succès
    * Information
    * Danger	
    * Avertissement
    * Jumbotron

* Navigation :
    * Barre de navigation
    * Fil d’arianne
    * Pagination

* Grille :
    * Génération d'une grille
